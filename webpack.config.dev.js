const webpack = require('webpack');
const path = require('path');
const Merge = require('webpack-merge');
const BaseConfig = require('./webpack.config.base');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

const distPath = path.join(__dirname, '/public/');

module.exports = function(env) {
    return Merge(BaseConfig, {
        devtool: 'eval',
        entry: {
            app: [
                'babel-polyfill',
                'webpack-dev-server/client?http://localhost:3000',
                './app/index'
            ]
        },
        output: {
            path: path.join(__dirname, distPath),
            pathinfo: true,
            publicPath: 'http://localhost:3000/' + distPath + '/',
            filename: '[name].js',
            libraryTarget: 'var',
            library: 'exports',
            sourceMapFilename: '[name].js.map'
        },
        plugins: [
            new CaseSensitivePathsPlugin(),
            new DashboardPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('development')
                }
            })
        ],
        devServer: {
            port: 3000,
            headers: {
                "Access-Control-Allow-Origin": "*",
            },
            overlay: {
                warnings: false,
                errors: true
            },
            noInfo: true,
            hot: true,
            inline: true,
            contentBase: distPath,
            historyApiFallback: true
        },
        module: {
            rules: [
                {
                    test: /^(?!.*\.spec\.jsx?$).*\.jsx?$/,
                    enforce: 'pre',

                    exclude: /(vendor|node_modules|dist)/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            presets: ['react', 'es2015', 'es2016', 'es2017', 'stage-0'],
                            plugins: ['react-hot-loader/babel']
                        }
                    }, {
                        loader: 'eslint-loader',
                        options: {
                            emitWarning: true,
                            configFile: '.eslintrc.js'
                        }
                    }]
                }
            ]
        }
    })
}
