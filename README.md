# React / Django Boilerplate

This project aims to have all the things needed already put together for having django
serve a react based application.

### React Changes

When using this project to set yourself up please be aware that there are a couple of
things you will need to change.

1. `app/`

    This directory is where the application code lives.  If `app` is a name that suits
    you then there is nothing to do.  However, you can choose what ever name makes sense
    to you.  Simply rename the folder.

1. `webpack.config.base.js`

    You will need to edit this file in the event you have renamed the `app` directory.
    There are two touch points:

    a. Line 16, the `entry` object's `app` property needs to be the path to the top level `index.js`
    
    b. Line 45, the `resolve` object's `modules` property needs an entry which is the path to the app

## Django Changes

1. Add `django-webpack-loader` as a django dependency
1. Add `webpack_loader` as an application in your settings
1. Configure `webpack_loader`

        WEBPACK_LOADER = {
            'DEFAULT': {
                'BUNDLE_DIR_NAME': 'path_to_where_you_put_this_repo/public/',
                'STATS_FILE': 'path_to_where_you_put_this_repo/webpack-stats.json'
            }
        }
1. Add the following to where you have your templates
    
    1. `react_scripts.html`
    
            {% if debug %}
                {% include "path_to_react_scripts_dev.html" %}
            {% else %}
                {% include "path_to_react_scripts_prod.html" %}
            {% endif %}

    1. `react_scripts_dev.html`

            {% load static from staticfiles %}
            {% load render_bundle from webpack_loader %}

            {% render_bundle 'vendor' %}
            {% render_bundle 'app' %}

    1. `react_scripts_prod.html`

            {% load static from staticfiles %}
            <script src="{% static 'app/public/vendor.js' %}" type="text/javascript"></script>
            <script src="{% static 'app/dist/app.js' %}" type="text/javascript"></script>




